using System;

namespace Test {
    public class MyClass {
        public MyClass () {
            string DatabasePath {
                get {
                    var sqliteFilename = "TodoSQLite.db3";
#if __IOS__
                    string documentsPath = Environment.GetFolderPath (Environment.SpecialFolder.Personal);
                    string libraryPath = Path.Combine (documentsPath, "..", "Library");
                    var path = Path.Combine(libraryPath, sqliteFilename);
#else
#if __ANDROID__
                    string documentsPath = Environment.GetFolderPath (Environment.SpecialFolder.Personal);
                    var path = Path.Combine(documentsPath, sqliteFilename);
#else
                    // WinPhone
                    var path = Path.Combine (ApplicationData.Current.LocalFolder.Path, sqliteFilename);
                    //TODO: Add Windows 8.1 and UWP cases here!
#endif
#endif
                    return path;
                }
            }
        }
    }
}
