\chapter{Introduction}

Every successful mobile application evolves from a great idea. But having a great idea itself is not a guarantee for success. A major factor of the success of the mobile application depends on which platforms it will be available. So, the first step is to decide which mobile platform(s) the application should be developed for which is called the target platform(s). In \Cref{fig:marketshareos}, a diagram is shown which summarizes the market share of different mobile \glspl{os} collected by Gartner Inc.\footnote{http://www.gartner.com/} over the last two years. Undeniably Android and iOS should be targeted as platforms for a successful application and to reach a wide user spectrum. Microsoft's Windows Phone share seems to be more on a downfall and negligible. The insignificant others are Blackberry, formerly known as \gls{rim}, and older \gls{os} like Nokia's Symbian or Samsung's Bada. So the major platforms an application should support are Android, iOS and in the rare case, \gls{uwp}.


\begin{figure}[!ht]
	\centering
	\pgfplotstableread[col sep=semicolon]{data/market_share.csv}\datatable
	
	% -- sharp plot
	\begin{tikzpicture}
	\begin{axis}[
	legend pos = outer north east,
	legend cell align = left,
	xlabel={Quarter},
	ylabel={},
	yticklabel={\pgfmathprintnumber\tick\%},
	ymin = 0,
	ymax = 100,
	xtick=data,
	xticklabels from table={\datatable}{Quarter},
	x tick label style={rotate=90, anchor=east}
	]
	\addplot [smooth, color=red, mark=*] table [x expr=\coordindex, y=Android] {\datatable};  
	\addplot [smooth, color=orange, mark=*] table [x expr=\coordindex, y=iOS] {\datatable};  
	\addplot [smooth, color=blue, mark=*] table [x expr=\coordindex, y=Windows] {\datatable};  
	\addplot [smooth, color=green, mark=*] table [x expr=\coordindex, y=Other] {\datatable};  
	\legend{Android, iOS, Windows, Other}
	\end{axis}
	\end{tikzpicture}
	\caption[Market share of mobile \gls{os}]{Market share of mobile \gls{os} \footnotemark} \label{fig:marketshareos}
\end{figure}

\footnotetext{Sources: \autocite{Gartner2014Q1}, \autocite{Gartner2014Q2}, \autocite{Gartner2014Q3}, \autocite{Gartner2014Q4}, \autocite{Gartner2015Q1}, \autocite{Gartner2015Q2}, \autocite{Gartner2015Q3}, \autocite{Gartner2015Q4}, \autocite{Gartner2016Q1}}


Targeting more than one platform inevitably leads to the question whether the application should be developed natively, meaning separately for each platform or if a \textbf{cross-platform mobile development framework} should be used. The first approach would lead to separate and independent implementations for each platform which means duplicated code throughout the targeted platforms. This also means duplicated risk for potential mistakes in the program's logic (bugs) and duplicated work for maintaining and improving the separate implementations for the applications. This seems to be a rather bad solution not only in terms of development efficiency but also for economical reasons.

Fortunately as already mentioned, \textbf{cross-platform mobile development frameworks} do exist. To simplify, they will be called frameworks in the further context. These frameworks lower the complexity of the development for a mobile application in the way that common logic code can be shared throughout the targeted platforms. This code will be referenced as shared code in the further reading.

There are a variety of frameworks which produce different types of applications. Related work defines these types differently, so to clarify the definitions for this work are as follows. \newline

\textbf{Native applications} are written in the target platforms language, e.g. Java for Android or Objective-C and Swift for iOS. They use the target platforms native \glspl{sdk} and therefore have full access to the provided platform \glspl{api}. Native applications are the fastest of all application types when implemented correctly in terms of \gls{ui} responsibility and scalability. The main disadvantage of native applications, as already mentioned, is that each application needs to be implemented for each platform separately which means a not negligible overhead and complexity in the development. \newline

\textbf{Web applications} are written with \gls{html}, \gls{css} and \gls{js}. A web application is basically an optimized version of a website for the mobile browser. The web application opens in a browser and does not need to be installed onto the phone by the user and is therefore totally platform-independent (as long as the phone has a web browser, the application can be used). A web application responds rather slow to user input as the \gls{ui} is always loaded from the server. However, the advantage is this kind of application only needs to be developed once. Unfortunately, a web application cannot utilize all platform-specific features even though browser developers are pushing more and more features and \glspl{api} to their browsers, such as access to the \gls{gps} and the camera. \newline

\textbf{Hybrid web applications} could be described as websites wrapped within a native browser element. As this native browser element, a so-called web view, is part of an application, these types of applications have to be installed on the device. Therefore, they can have access to the underlying hardware through the \glspl{api} provided by the framework, depending, of course, on the maturity of the framework. Applications of this type are mostly developed in \gls{html}, \gls{js} and \gls{css}. As they use a web view they still tend to be slower compared to native applications, except that the \gls{ui} is stored locally. \newline

\textbf{Hybrid generated applications} are applications which could be almost seen as native applications. The user interface is generated from platform independent user interface definitions and resembles native \gls{ui} elements. The logic is implemented platform independently in a language determined by the framework (e.g. c\# or ruby) and either compiled to the native language or to an \gls{il}, which is then interpreted on the device. These frameworks tend to fully support the targeted platform in terms of the hardware \glspl{api} and native \gls{ui} elements. \newline

Beside these terms, a general knowledge of mobile application development and software engineering are the presumed fundamental knowledge for the further reading and understanding of this thesis.

\section{Goal}
The main goal of this thesis is to determine a framework which fulfills the requirements of modern mobile application development and stands out from the other frameworks. Therefore, the requirements for mobile application development need to be defined and potential frameworks need to be researched and evaluated. 

A framework should be chosen which fulfills at best the specified requirements. Additionally, supporting material should be provided to help companies to determine whether to use the framework or to follow the native approach when planning the development of a new application or when thinking about extending an existing application to another platform.

Together with the elaborated material the application \textit{Spritradar} should be evaluated for the extension to the Android platform. The development process as well as the gained insights on the framework should be protocoled.

\section{Related Work} 

The field of cross-platform mobile development frameworks can be considered quite unstable when looking at the past three years. With the technical progress and all the research and development that is done in this field, almost monthly a new framework is published or announced in the news pretending to be the next 'big thing' which solves all problems. Therefore, existing papers and sources, even when less than one year old, are unfortunately quite outdated. However, they still can be considered valuable sources to compare the research and results of this thesis.

The requirements for mobile application development did not really change in the past years so. Thereas results of \autocite{Palmieri2012a}, \autocite{Joorabchi2013}, \autocite{Dalmasso2013}, and \autocite{Ciman2014} can be considered when defining the requirements for \Cref{sec:methodrequirements}.

Some of the frameworks which will be mentioned later in \Cref{sec:resultsframeworks} were already evaluated by various sources. One of the more famous frameworks, \textit{PhoneGap}, was discussed by \autocite{Babu2012}, \autocite{Palmieri2012a} and \autocite{Francese2013} with the result that it's a competent framework while having disadvantages in the performance and the "look \& feel" when comparing the resulting application to the natively implemented application. 

The different types of mobile applications as defined before are discussed and defined similarly and more detailed by \autocite{Xanthopoulos2013} and \autocite{khanna2016getting}. Furthermore, in the study from \autocite{Latif2016} with the described \gls{mda} approach another type of cross-platform mobile application generating framework is mentioned.

An interesting appraisal was done by \autocite{Charkaoui2014} as the study suggests the use of different cross-platform mobile development frameworks depends strongly on the individual targeted developers such as web developers or software engineers.

Testing cross-platform applications for inconsistencies which were developed with Xamarin is part of a study from \autocite{Boushehrinejadmoradi2016}. According to the study, applications developed with cross-platform mobile development frameworks can have different behaviors on different platforms when calling library functions with the exact same arguments. The reason for those inconsistencies is how cross-platform mobile development frameworks handle the native \glspl{api}. The study found those inconsistencies with the tool \textit{X-Checker} which was developed as part of the study and reported those inconsistencies to the Xamarin bug-tracker. It seems that most of the reported bugs were fixed shortly after being reported.
