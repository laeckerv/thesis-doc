\chapter{Implementation} \label{chap:implementation}
Following the outcome of the evaluation described in \Cref{chap:discussion}, guidelines for future projects can be implemented. \Cref{sec:decision_guide} should give some insight into determining whether Xamarin should be used for the development of a certain mobile application/design. Furthermore, if the outcome is to use Xamarin it helps to determine whether to use Xamarin Classic or Xamarin Forms.

\Cref{sec:dev_guide} should give new developers a hint on how to use Xamarin. The last section of this chapter (\Cref{sec:spritradar_app}) describes the application of the two guidelines for the development of the Android version of \textit{Spritradar} (\Cref{chap:spritradar}).

\section{Applying  Xamarin} \label{sec:decision_guide}
Closing back in on the introduction, the next step after the decision to write an application for multiple platforms or maybe even just for one platform is to decide whether to use the native or cross-platform development approach. \Cref{fig:decisiontree} should give suggestions to the decision makers, e.g. project manager or developer, who have to decide between the native, Xamarin Classic and Xamarin Forms approach.

The first question that has to be asked is if the client, which could be simply yourself, insists on developing the application completely native for each of the targeted platforms. This would abruptly eliminate the question of whether to use a cross-platform mobile development framework.

If nothing comes in the way of developing with a cross-platform development framework, then Xamarin should be used. The only question left at that point is which of the approaches that Xamarin offers should be used. 

If for the time being only one platform is planned to be supported then Xamarin Classic should be used. The reason is because this approach doesn't come with restrictions of implementing the \gls{ui}. Any logic that could be reused for eventual implementation of an additional platform should be evaluated and, whenever possible, be abstracted in the form of shared code (either Shared Project or \gls{pcl}). 

If it is already planned to support multiple platforms, the next step is to figure out whether the application requires much \gls{ui} customization or has many \gls{ui} interactions or animations. As Xamarin Classic fully supports the native \glspl{sdk}, those customizations and animations are done straightforwardly and more easily, while with Xamarin Forms more manual implementations would be necessary or might not even be possible.

If the applications relies on the usage of multiple native libraries, e.g. CocoaPods for iOS or Java libraries (jars) for Android, it is also strongly recommended to use Xamarin Classic. With Xamarin Forms, the libraries's functionalities would have to be further abstracted for each of the platforms to allow the usage from within the platform independent project. 

As seen in \Cref{fig:decisiontree} and also the previous paragraphs, there is basically no argument for developing natively or, to put it the other way, to not use Xamarin as cross-platform mobile development framework. The benefits clearly outweigh the disadvantages.

\begin{figure}[!ht]

	\setlength{\fboxsep}{1em}
	\centering
	\fbox{%
		\begin{tikzpicture}[node/.style={draw,},]
		\node [node, text width=5cm] (A) {Client insists on native development?};
		\path (A) ++(-90:\nodeDist) node [node, text width=5cm] (B) {Only one platform requested? (yet)};
		\path (B) ++(-90:\nodeDist) node [node, text width=5cm] (C) {Application requires heavy UI customization and interaction?};
		\path (C) ++(-90:\nodeDist) node [node, text width=5cm] (D) {Use of multiple native libraries e.g. CocoaPods?};
		
		\path (D) ++(-135:2.0\nodeDist) node [node] (E) {\textbf{Native}};
		\path (D) ++(-90:1.42\nodeDist) node [node] (F) {\textbf{Xamarin Forms}};
		\path (D) ++(-45:2.0\nodeDist) node [node] (G) {\textbf{Xamarin Classic}};
		
		\draw[->] (A) -- (B) node [right,pos=0.5] {no}(A);
		\draw[->]  (B) -- (C) node [right,pos=0.5] {no}(A);
		\draw[->]  (C) -- (D) node [right,pos=0.5] {no}(A);
		
		\draw[->]  (A) -| (E) node [left, pos=0.6] {yes} (A);
		\draw[->]  (B) -| (G) node [right, pos=0.55] {yes} (B);
		\draw (C) -| (G) node [right, pos=0.55] {yes} (C);
		\draw (D) -| (G) node [right, pos=0.55] {yes} (D);
		
		\draw[->]  (D) -- (F) node [right, pos=0.5] {no} (A);
		\draw[->]  (D) -- (G) node [right, pos=0.5] {yes} (A);
		\end{tikzpicture}
	}
	\caption{Which development approach should be used?}
	\label{fig:decisiontree}
\end{figure}

\section{Spritradar for Android}\label{sec:spritradar_app}
According to the results of the evaluation, it was quite obvious that the Android version of \textit{Spritradar} has to be developed with the use of a cross-platform mobile development framework or, more precisely, with Xamarin. The question left to be answered was whether to use the Xamarin Classic or the Xamarin Forms approach. 

The application of the decision tree in \Cref{sec:decision_guide} revealed that Xamarin Forms is the right choice for the implementation of the Android application. This decision is based on the fact that the iOS application has neither a high customization of the \gls{ui} nor does it using difficult animations. 

One problem, though, was that the client (my bosses) weren't too sure about this approach as it is a wide-spread opinion throughout the Internet that Xamarin Forms is still in an early development stage and brings more headaches than utilization. 

At this point, I needed to agree that there was one reason why Xamarin Classic could be preferred over Xamarin Forms for the \textit{Spritradar} applications case. This is due to the fact that the map element and the customized map marker could end in an unsolvable problem which has to be dealt with. However, with the results of the evaluation in mind, they (the bosses) gave Xamarin Forms a chance with the strict restraint to switch to Xamarin Classic if any sign of running into a dead end with Xamarin Forms appears.

The impressive part of developing the Android application with Xamarin Forms is the easiness of writing two applications with only focusing on one code base. Implementing the map view (Xamarin.Forms.Maps\footnote{\url{https://www.nuget.org/packages/Xamarin.Forms.Maps}}) and the slide out menu together with an animation in the bottom was no problem at all.

Implementing the database interaction (copy existing database/store/load) and the calls to web services was uncomplicated using SQLite.Net\footnote{\url{https://www.nuget.org/packages/sqlite-net-pcl/}}, the extensions\footnote{\url{https://www.nuget.org/packages/SQLiteNetExtensions}} and SQLite.Net.Async\footnote{\url{https://www.nuget.org/packages/SQLite.Net.Async-PCL/}}. 

Additional packages which were used for receiving the users location was the Geolocator Plugin\footnote{\url{https://www.nuget.org/packages/Xam.Plugin.Geolocator}} and the Settings Plugin\footnote{\url{https://www.nuget.org/packages/Xam.Plugins.Settings/}}. The later abstracts the key-value storage for each platform to be accessible in the platform-independent project.
 
As already mentioned before, the abstraction of the map of Xamarin was quite a problem regarding the possibility to customize the markers, but throughout the thesis solutions have already been worked out to implement them platform-dependently. This is an exceptional example of how Xamarin Forms allows platform-dependent representations of \gls{ui} elements being accessed from the platform-independent code. The implementation of the map markers is done in form of a custom renderer for each platform.

\begin{figure}[!ht]
	\centering
	\begin{subfigure}[b]{0.45\textwidth}
		\includegraphics[width=\textwidth]{img/xamarinf_android_app}
		\caption{\textit{Spritradar} in Xamarin Forms on Android}
		\label{fig:spritradar_android_app}
	\end{subfigure}
	\hfill %add desired spacing between images, e. g. ~, \quad, \qquad, \hfill etc. 
	\begin{subfigure}[b]{0.45\textwidth}
		\includegraphics[width=\textwidth]{img/xamarinf_ios_app}
		\caption{\textit{Spritradar} in Xamarin Forms on iOS}
		\label{fig:spritradar_ios_app}
	\end{subfigure}
	\caption{Xamarin Forms Spritradar}\label{fig:spritradar_app}
\end{figure}

As of yet, the renderer was only implemented for Android because the Android application has priority in the development process (\Cref{fig:spritradar_app}) due to the fact that a native iOS application already exists. The platform-dependent implementations of the iOS application will be proceeded as soon as the Android application is ready to be released. This will eventually result in only having to maintain a single \textit{Spritradar} Xamarin Forms application.

\section{Supporting the Developer} \label{sec:dev_guide}
This section should help new Xamarin Classic and Xamarin Forms developers to avoid common mistakes and give solutions for common problems for instance the localization and internationalization (\Cref{sec:l10nl18n}) and the use of legacy C/C++ libraries with Xamarin (\Cref{sec:legacyc}).

\subsection{Localization \& Internationalization}\label{sec:l10nl18n}
Sometimes it can be necessary to support multiple languages within one mobile application. Xamarin Forms has a built-in way of solving this cross-platform-wise. For Xamarin Classic, different approach has to be chosen.

\subsubsection{Xamarin Forms}
Xamarin Forms has a built-in way of dealing with different languages. The text for the languages has to be defined within a resource file (.resx). For each language, such a resource file containing the translations has to be created. The resource file is structured like an \gls{xml} file. Each string that needs to be localized is defined with a unique name, the value and optionally a comment. The localized strings can then be accessed from the C\# code as well as from the \gls{xaml} code. An example can be found in the Xamarin documentation.\footnote{\url{https://developer.xamarin.com/guides/xamarin-forms/advanced/localization/}}

\subsubsection{Xamarin Classic}
Compared to the aforementioned way with how Xamarin Forms deals with the localization, Xamarin Classic needs additional manual effort. As Xamarin Classic uses the native resource files for the strings which can be localized, these files need to be provided for each platform.

An easy solution is to define the strings in a platform-independent format and generate the required resource files for the different platforms. Formigas GmbH has this kind of solution implemented in Java. It parses the content of an \gls{xml} file containing the translations and generates the corresponding language resource files for Android and iOS.

Another solution is Resxible\footnote{\url{https://github.com/apcurium/resxible/}} which parses the content of an .resx file and generates the corresponding language resource files for Android and iOS. A benefit to this solution is that it is written in C\# and that it could just be added to each project via Nuget. Therefore the organization of this additional tool would be more practical.

\subsection{C/C++ libraries}\label{sec:legacyc}
For some developers, it may be interesting that not only Java or Objective-C libraries but also C/C++ functions and libraries can be called from C\# Xamarin code. A well written blog post from \autocite{Lothrop2016} describes the whole process of how to wrap and call the library functions from the Xamarin Android and Xamarin iOS project.

\subsection{Unit Tests}
Xamarin allows to write unit tests with the use of NUnit. Logically, platform dependent-functionality can only be tested on the corresponding platform. Therefore tests are written individually within a platform-specific test project. This is basically a test application which can be run on the corresponding platform. Once deployed to the emulator or device the application can run the defined tests and present the results on-screen (\Cref{fig:xamarin_android_unittest}).

\begin{figure}[!ht]
	\centering
	\begin{subfigure}[b]{0.45\textwidth}
		\includegraphics[width=\textwidth]{img/xamarin_android_unittest}
		\caption{Xamarin Android Unit Test}
		\label{fig:xamarin_android_unittest_screen}
	\end{subfigure}
	\hfill %add desired spacing between images, e. g. ~, \quad, \qquad, \hfill etc. 
	\begin{subfigure}[b]{0.45\textwidth}
		\includegraphics[width=\textwidth]{img/xamarin_android_unittest_results}
		\caption{Xamarin Android Unit Test Results}
		\label{fig:xamarin_android_unittest_result}
	\end{subfigure}
	\caption{Xamarin Android Unit Test}\label{fig:xamarin_android_unittest}
\end{figure}

\subsection{UI Tests}
Additionally to the unit tests, Xamarin offers with the Test Cloud\footnote{\url{https://www.xamarin.com/test-cloud}} and the \gls{ui} Tests \footnote{\url{https://developer.xamarin.com/guides/xamarin-forms/deployment,_testing,_and_metrics/uitest-and-test-cloud/}}\footnote{\url{https://developer.xamarin.com/guides/testcloud/uitest/}} a thorough test suite for \gls{ui} tests including the required infrastructure. From the marketing ads, the description and the examples on the website, this solution emphasizes that it is a reliable tool for testing cross-platform applications. Unfortunately, given the limited time of this thesis, I wasn't able to experiment with this promising feature.

\subsection{MVVM}
If you start to develop cross-platform mobile applications with Xamarin Forms there is no way around the \gls{mvvm} pattern. However, even with Xamarin Classic it can be a great benefit to develop the cross-platform mobile application with the \gls{mvvm} pattern resulting in the separation of the implementation for the \gls{ui} and the business logic.

Of course Xamarin iOS and Xamarin Android don't support this pattern out-of-the-box, but there are a couple of powerful libraries that help to realize the \gls{mvvm} pattern in a Xamarin Classic project.

One of them is MvvmCross\footnote{\url{https://github.com/MvvmCross}}. This framework also has a comprehensible tutorial for the Xamarin Classic approach. The tutorial\footnote{\url{https://github.com/MvvmCross/MvvmCross}} is structured the in following way:

\begin{itemize}
	\item The Core Project \footnote{\url{https://github.com/MvvmCross/MvvmCross/wiki/Tip-Calc---The-Core-Project}}
	\item A Xamarin.Android view project\footnote{\url{https://github.com/MvvmCross/MvvmCross/wiki/Tip-Calc-A-Xamarin.Android-UI-project}}
	\item A Xamarin.iOS view project\footnote{\url{https://github.com/MvvmCross/MvvmCross/wiki/Tip-Calc-A-Xamarin.iOS-UI-project}}
\end{itemize}

The tutorial a simple calculator that takes advantage of the \gls{mvvm} pattern for the implementation of the Xamarin Android, iOS and even the \gls{uwp} application.