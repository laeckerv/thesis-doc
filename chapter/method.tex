\chapter{Method}

This chapter gives an overview of how the requirements (\Cref{sec:methodrequirements}) are obtained to evaluate (\Cref{sec:methodevaluation}) the collected frameworks (\Cref{sec:methodframeworks}).

\section{Requirements \& Survey} \label{sec:methodrequirements}
Even though my background in the development of mobile applications compared to senior developers with years of experience could be assessed rather low the first step is to collect requirements through personal experience. Additionally, the work from \autocite{Heitkotter2013} and \autocite{Joorabchi2013} is considered for the collection. The next step is to create a survey to validate the collected requirements by more experienced developers. 

\begin{figure}[!ht]
	\centering
	\includegraphics[width=0.8\textwidth]{img/survey_example}
	\caption{Google Forms Multiple Choice Question}
	\label{fig:example_survey}
\end{figure}

The structure and the appearance of the survey, with only a couple of multiple choice questions and some text inputs, should be kept rather simple and therefore Google Forms \autocite{GoogleForms2016} is the tool of choice. 

The survey is divided into four sections "Prerequisites," "General," "Development," and "Application." Each section is enclosed with a text field where the interviewee can optionally add eventually missing requirements or has the chance to give feedback to the listed ones.

Especially in the first section, the questions are simple yes or no (yes/no) questions and multiple choice (multi) questions as shown in \Cref{fig:example_survey}. In the other sections, most of the questions are single choice (rate) questions where the interviewee rates a certain requirement. The rating is done with the following literal scale: 

\begin{itemize}
	\item really important
	\item important
	\item neutral
	\item less important
	\item not important
\end{itemize}

 As already mentioned, the end of each section is concluded with an input field (text) for further feedback of the interviewee.

\myparagraph{Prerequisites}

The "Prerequisites" section questions are targeting the background of the interviewee in the field of mobile development and the usage of cross-platform mobile development frameworks. These question should help to classify the answers of the interviewee given in the following sections. The text in the brackets gives information about the type of the question within the survey.

\begin{itemize}
	\item (yes/no): Do you develop mobile applications?
	\item (multi): Which mobile platforms do you program applications for?
	\item (yes/no): Did/do you use a cross-platform mobile development framework?
	\item (multi): Which cross-platform mobile development framework did/do you use?
	\item (text): What is your feedback of using a cross-platform mobile development framework?
\end{itemize}

\myparagraph{General}
The "General" section asks the interviewee to rate the importance of more general requirements when considering a cross-platform mobile development framework. \\

\textbf{How important is it for you that a framework...}

\begin{itemize}
	\item (rate): is available for free, not including optional subscriptions for additional support? \textbf{(Pricing)}
	\item (rate): supports more than two platforms? \textbf{(Supported target platforms)} 
	\item (rate): supports iOS as target platform? \textbf{(Support of iOS)}
	\item (rate): supports Android as target platform? \textbf{(Support of Android)}
	\item (rate): supports Windows Phone as target platform? \textbf{(Support of Windows Phone)}
	\item (rate): has a native-like performance? \textbf{(Performance)}
	\item (rate): doesn't restrict the developer in the freedom of design? (\gls{ui}) \textbf{(Freedom of design)}
	\item (rate): offers support from the vendor? \textbf{(Support B2B)}
	\item (rate): offers support from the community? \textbf{(Support Community)}
	\item (rate): has a clean and well structured documentation? \textbf{(Documentation)}
\end{itemize}


\myparagraph{Development}
The "Development" section targets the requirements for the development process of a mobile application. It can be divided into the requirements for the workflow and more miscellaneous requirements for the development.

\myparagraph{Workflow:}

\textbf{How important is it for you that a framework offers a simple workflow for the...}

\begin{itemize}
	\item (rate): Initial setup of the project?
	\item (rate): Development of the application?
	\item (rate): Deployment of the application to the emulator?
	\item (rate): Deployment of the application to the device?
	\item (rate): Release of the application to the targeted platform stores?
	\item (rate): Internationalization and localization of the application?
\end{itemize}

\myparagraph{Miscellaneous:}

\textbf{How important is it for you that a framework...}

\begin{itemize}
	\item (rate): comes with a competent \gls{ide}? \textbf{(\gls{ide})}
	\item (rate): uses a wide spread and current programming language? \textbf{(Programming language)}
	\item (rate): has a steep\footnote{A steep learning curve means gaining lot of knowledge over short time.} learning curve? \textbf{(Learning Curve)}
	\item (rate): supports the integration of tests? \textbf{(Integration of tests)}
	\item (rate): has a quick adaption to updates in the native \glspl{sdk}? \textbf{(Quick adaption to updates in the native \glspl{sdk})}
\end{itemize}

\myparagraph{Application}

The "Application" section targets the requirements for the mobile application itself. It can be divided into requirements for the interaction with platform resources and the requirements to support other certain commonly used features. \newline

\textbf{How important is it for you that a framework supports the interaction with a...}

\begin{itemize}
	\item (rate): Database?
	\item (rate): Web service?
	\item (rate): Hardware (\gls{gps}, \gls{bt}, \gls{nfc})?
	\item (rate): Device status (battery, data connection)?
\end{itemize}

\textbf{How important is it for you that a framework supports...}

\begin{itemize}
	\item (rate): Third party libraries (CocoaPods, Jar)?
	\item (rate): Notifications?
	\item (rate): User tracking?
	\item (rate): In-App messaging?
	\item (rate): In-App purchasing? 
\end{itemize}


\section{Market Analysis} \label{sec:methodframeworks}

After collecting the requirements the next step before evaluating the different frameworks is to actually collect potential candidates. Therefore, the market of cross-platform mobile development frameworks has to be analyzed.

As former papers suggest a framework that produces a hybrid web application as the future of cross-platform mobile development, at least one of the frameworks which produces this type of application has to be selected for further evaluation.

Furthermore, the focus of this work should be put on freely available frameworks and frameworks backed by matured and reliable companies.

As Android and iOS use Java, Objective-C and Swift, which are three mature programming languages, the evaluation should also put focus on the used languages for the development.

Four of the collected frameworks should be preselected for the further evaluation regarding those aforementioned requirements.

\section{Evaluation} \label{sec:methodevaluation}
The evaluation is done in two steps. First a prototype applications is implemented for each of the four preselected frameworks. This should provide a deeper insight in how each of the frameworks works compared to a purely theoretical evaluation. Additionally, this approach should enormously help to figure out which features each of the frameworks offer for the implementation of a real-world application. The real-world application, respectively a prototype application, and its features are further explained in \Cref{sec:methodprototype}.

With the gained insights, each framework is then evaluated according to the collected and, at that point, validated requirements from \Cref{sec:methodrequirements}.

\subsection{Prototype Application} \label{sec:methodprototype}
The main idea of the prototype is derived from the application \textit{Spritradar}\footnote{\url{http://www.spritradar.de/}}\footnote{\url{https://itunes.apple.com/de/app/spritradar-immer-gunstig-tanken!/id963701083?mt=8}}. The benefit of using \textit{Spritradar} as a template for the prototype is that the server-side component is already fully functional and available. As the timespan for the evaluation of each framework is quite limited, the main features of \textit{Spritradar} are extracted and defined for the prototype application.

\subsubsection{Spritradar}\label{chap:spritradar}
\textit{Spritradar} is an application that is developed by Formigas GmbH. Its main purpose is to show the cheapest petrol stations within a certain radius of the users location. Therefore, a map is shown and markers which represent the petrol stations are drawn on the map (\Cref{fig:native_ios_app_mapl}). Each marker shows the current petrol price for each station. If the user clicks on a marker more details such as the opening hours and the prices for each type of gas available at the petrol station are shown (\Cref{fig:native_ios_app_detail}). Furthermore a route to the petrol station is calculated and visualized on the map. 

The user also has the possibility to search for an address. If the address is found the route to the address is calculated and the cheapest petrol stations along the route are visualized on the map (\Cref{fig:native_ios_app_route}).

\begin{figure}[!ht]
	\centering
	\begin{subfigure}[b]{0.3\textwidth}
		\includegraphics[width=\textwidth]{img/native_ios_application1}
		\caption{Map view}
		\label{fig:native_ios_app_mapl}
	\end{subfigure}
	~ %add desired spacing between images, e. g. ~, \quad, \qquad, \hfill etc. 
	%(or a blank line to force the sub figure onto a new line)
	\begin{subfigure}[b]{0.3\textwidth}
		\includegraphics[width=\textwidth]{img/native_ios_application2}
		\caption{Petrol station detail}
		\label{fig:native_ios_app_detail}
	\end{subfigure}
	~ %add desired spacing between images, e. g. ~, \quad, \qquad, \hfill etc. 
	%(or a blank line to force the sub figure onto a new line)
	\begin{subfigure}[b]{0.3\textwidth}
		\includegraphics[width=\textwidth]{img/native_ios_application3}
		\caption{Route}
		\label{fig:native_ios_app_route}
	\end{subfigure}
	\caption{Native iOS Application}\label{fig:native_ios_app}
\end{figure}

All the required data for the petrol stations including price updates, opening hours and locations are fetched from the official market transparency unit \autocite{Bundeskartellamt2016} and stored on the company's server. The server backend offers access to the data via a web service.

Together with the application, a widget is installed on the device which shows the cheapest price in the area (\Cref{fig:native_ios_app_widget}). This widget is updated automatically when a new price is available.

Another benefit of this application over its competitors is the price tag of the closest petrol station shown on the applications icon. (\Cref{fig:native_ios_app_badge}). This number on the icon is called badge. This feature helps the user to see the cheapest price in the area without the need to open the application.

\begin{figure}[!ht]
	\centering
	\begin{subfigure}[b]{0.4\textwidth}
		\centering
		\includegraphics[width=\textwidth]{img/native_ios_widget}
		\caption{Widget}
		\label{fig:native_ios_app_widget}
	\end{subfigure}
	~ %add desired spacing between images, e. g. ~, \quad, \qquad, \hfill etc. 
	%(or a blank line to force the sub figure onto a new line)
	\begin{subfigure}[b]{0.3\textwidth}
		\includegraphics[width=\textwidth]{img/native_ios_badge_icon}
		\caption{Icon badge}
		\label{fig:native_ios_app_badge}
	\end{subfigure}
	\caption{Native iOS Application Features}\label{fig:native_ios_app_features}
\end{figure}

On major disadvantage of \textit{Spritradar} which is also a reason why this thesis was written is the fact that the application is currently only available for iOS and the next step would be to implement the Android application. The question if and which framework to use is hopefully answered by the end of this thesis.

\subsubsection{Prototype}
The following functionality is extracted from the \textit{Spritradar} application for iOS and defines the prototype application for the practical evaluation of the frameworks. For the evaluation of each framework this functionality should be implemented with each framework.

Due to the already immense initial time needed to get to know each framework, the applications appearance, functionality and complexity should be kept at a bare minimum. This also means that the implementation should only be  considered as a proof of concept for the realization of each feature. This is also a reason for declaring some features as optional. If a framework offers an easy way of implementing the mandatory features, the optional ones can be implemented (if possible) and considered as additional criteria when it comes to the evaluation.

\paragraph{Mandatory}

\begin{itemize}
	\item A map element should be visible
	\item The user location should be visualized on the map
	\item The location and data for the pins should be loaded from a web service
	\item The location and data for the pins should be stored in a local sqlite database
	\item When clicking on a pin, a route from the current location of the user to the pin should be calculated and visualized
\end{itemize}

\paragraph{Optional}

\begin{itemize}
	\item A badge and a widget should be implemented for iOS
	\item Push notifications should be sent locally when an update of the prices for the visible petrol stations is detected.
	\item External libraries should be used, e.g. HockeyApp (iOS/Android), CocoaPods (iOS), Java Library (Android)
	\item Animations should be tested
\end{itemize}
